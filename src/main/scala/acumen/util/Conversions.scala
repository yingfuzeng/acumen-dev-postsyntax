package acumen
package util

import scala.math._
import Errors._
import acumen.interpreters.enclosure.Rounding
import acumen.interpreters.enclosure.Interval

object Conversions {

  def extractDouble(v:GroundValue) : Double = 
    v match {
      case GInt(i)    => i.toDouble
      case GDouble(x) => x
      case e: GRealEnclosure if (e isThin) => e.range.loDouble
      case _ => throw GroundConversionError(v, "double")
    }
  
  def extractDouble(v:Value[_]) : Double =
    v match {
      case VLit(gv) => extractDouble(gv)
      case _        => throw ConversionError(v, "double")
    }

  def extractDoubleNoThrow(v:Value[_]) : Double = 
    try {
      v match {
        case VLit(gv) => extractDouble(gv)
        case _        => throw ConversionError(v, "double")
      }
    } catch {
      case _ => Double.NaN
    }

  def extractDoubles(vs:List[Value[_]]) : List[Double] =
    vs map extractDouble

  def extractDoubles(v:Value[_]) : List[Double] =
     v match {
      case VList(vs) => extractDoubles(vs)
      case VVector(vs) => extractDoubles(vs)
      case _        => throw NotACollection(v)
    }

  def extractInt(v:GroundValue) : Int = 
    v match {
      case GInt(i)    => i
      case _ => throw GroundConversionError(v, "int")
    }
  
  def extractInt(v:Value[_]) : Int =
    v match {
      case VLit(gv) => extractInt(gv)
      case _        => throw ConversionError(v, "int")
    }

  def extractInterval(v: GroundValue)(implicit rnd: Rounding): Interval =
    v match {
      case GInterval(i) => i
      case GInt(i) => Interval(i, i)
      case GDouble(d) => Interval(d, d)
      case e: GRealEnclosure => e.range
      case _ => throw GroundConversionError(v, "interval")
    }
  
  def extractInterval(v:Value[_])(implicit rnd: Rounding) : Interval =
      v match {
      case VLit(gv) => extractInterval(gv)
      case _        => throw ConversionError(v, "interval")
  }
  // Check that every row has the same length
  def isMatrix(m:List[Value[_]]):Boolean ={
    try{
    	val rowLength = m(0) match{
    	case VVector(vs) => vs.length
    	case _ => error("Row 0 is not a vector")
    	}
    	m.forall(x => x match{
    	case VVector(vs) => vs.length == rowLength
    	case _ => false
    	})}
    catch{
      case _:Throwable => false
    }
  }
  def transMatrixArray(m:List[Value[_]]):Array[Array[Double]] = {
    if(isMatrix(m)){
      m.map(x => x match{
        case VVector(vs) => extractDoubles(vs).toArray
        case _ => error(m.toString + " is not a matrix")
      }).toArray
      
    }
    else
     Array(m.map(x => extractDouble(x)).toArray)
  }
  
  def transArrayMatrix(a:Array[Array[Double]]):List[Value[_]] = {
    val ls = a.map(x => x.toList).toList
    ls.map(x => VVector(x.map(y => VLit(GDouble(y)))))		  
  }

  def extractIntervals(vs:List[Value[_]])(implicit rnd: Rounding) : List[Interval] =
    vs map extractInterval
  
  def extractIntervals(v:Value[_])(implicit rnd: Rounding) : List[Interval] =
     v match {
      case VList(vs) => extractIntervals(vs)
      case VVector(vs) => extractIntervals(vs)
      case _        => throw NotACollection(v)
    }
  
	def extractSeed(v1:Value[_], v2:Value[_]) : (Int, Int) = {
		(extractInt(v1), extractInt(v2))
	}

  def extractId[Id](v:Value[Id]) : Id = {
    v match {
      case VObjId(Some(id)) => id
      case _ => throw NotAnObject(v)
    }
  }

}
